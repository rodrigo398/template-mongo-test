var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var MongoClient = require("mongodb").MongoClient; // Driver for connecting to MongoDB
var index = require('./routes/index');
var users = require('./routes/users');
var compression = require('compression');
var app = express();
app.use(compression());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', index);
//app.use('/users', users);

mongo = (req, res) => {
    conn.collection("users").find({}).toArray((err, data) => {
        if (err) {
            res.status(500);
            res.json(err);
        } else {
            res.json(data);
            console.log(data);
        }
    });
    conn.collection("users").insert({ nombre: "Mauricio" });
}

app.get('/mongo', mongo);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

MongoClient.connect("mongodb://mauricio:mauricio@ds135700.mlab.com:35700/templatemongo", function(err, db) {
    if (err) {
        console.log("Error: DB: connect");
        console.log(err);

        process.exit(1);
    }
    console.log("Connected to the database: " + "mongodb://mauricio:mauricio@ds135700.mlab.com:35700/templatemongo");
    conn = db;
});

var conn;

module.exports = app;